﻿using System;
namespace ProductNamespace
{
     public class Product
    {
        public Guid Id { get; private set; }
        public string Name { get; private  set; }
    
        public string Description { get; private set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int Price { get; set; }

        public int VAT { get; set; }

        public bool isValid()
        {
            //the time interval is not valid
            if (EndDate < StartDate) return false;

            // the product has expired
            return !(EndDate <= DateTime.Now);
        }

        public double ComputeTVA()
        {
            //the computed VAT formula
            return Price * (VAT / 100);
        }
              public Product(int newId, string newName, string newDescription, DateTime newStartDate, DateTime newEndDate, int newPrice, int newVAT) {
            if(newId < 0) 
            {
                throw new System.ArgumentException("id is not valid, <0", "newId");
            }
            this.Id = new Guid();

            if(newName.Length >51){
                throw new ArgumentException("name too long");
            }
            this.Name = newName;

            if(newDescription.Length >201){
            throw new ArgumentException("description too long");
            }
            this.Description = newDescription;

            this.StartDate = newStartDate;
            this.EndDate = newEndDate;

            if(newPrice < 0)
            {
                throw new System.ArgumentException("Parameter cannot be < 0", "newPrice");
            }
            this.Price = newPrice;

            if(newVAT < 0)
            {
                throw new System.ArgumentException("Parameter cannot be < 0", "newVAT");
            }
            this.VAT = newVAT;
        }

    }
}
